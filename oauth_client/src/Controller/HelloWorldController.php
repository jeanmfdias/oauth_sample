<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloWorldController
{
    #[Route('/')]
    public function index(): Response
    {
        return new Response('<h1>Hello World</h1>');
    }
}